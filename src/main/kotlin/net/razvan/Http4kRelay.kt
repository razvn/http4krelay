package net.razvan

import org.http4k.client.ApacheClient
import org.http4k.client.PreCannedApacheHttpClients
import org.http4k.cloudnative.env.Environment
import org.http4k.cloudnative.env.EnvironmentKey
import org.http4k.core.*
import org.http4k.events.Event
import org.http4k.events.EventFilters
import org.http4k.events.MetadataEvent
import org.http4k.events.then
import org.http4k.filter.ClientFilters
import org.http4k.filter.ResponseFilters
import org.http4k.lens.int
import org.http4k.lens.string
import org.http4k.server.ApacheServer
import org.http4k.server.asServer
import java.time.Clock

class RelayMessage(client: HttpHandler, remoteUrl: String, private val validHooks: List<String>) {
    private val cli = ClientFilters.SetBaseUriFrom(Uri.of(remoteUrl)).then(client)
    private val errorLens = Body.string(ContentType.TEXT_PLAIN).toLens()
    val relay: HttpHandler = { req: Request ->
        val hook = req.uri.path.substringAfterLast("/")
        if (validHooks.isEmpty() || hook in validHooks) {
            cli(req).removeHeader("Content-Length")
        }
        else errorLens("Unknown hook", Response(Status.BAD_REQUEST))
    }
}

fun main() {
    val portLens = EnvironmentKey.int().defaulted("PORT", 8080)
    val urlLens = EnvironmentKey.required("REMOTE_URL")
    // val urlLens = EnvironmentKey.defaulted("REMOTE_URL", "http://localhost:6060")
    val hooksLens = EnvironmentKey.defaulted("VALID_HOOKS", "")
    val printEvent = { e: Event ->
        if (e is MetadataEvent) {
             val timestamp = e.metadata["timestamp"] ?: Clock.systemUTC().instant()
            println("$timestamp ${e.event}")
        } else println(e)
    }
    val events =
        EventFilters.AddTimestamp()
            .then(printEvent)

    val env = Environment.ENV
    val validHooks = hooksLens(env).split(",")
    val port = portLens(env)
    val url = urlLens(env)

    // val client = JavaHttpClient()
    val client = ApacheClient(PreCannedApacheHttpClients.insecureApacheHttpClient())
    val app = ResponseFilters.ReportHttpTransaction {
        events(IncomingHttpRequest(it.request.uri, it.response.status.code, it.duration.toMillis()))
    }
        .then(
            RelayMessage(client, url, validHooks).relay
        )

    val relay =app
        .asServer(ApacheServer(port))
        .start()
    println("Server started on: ${relay.port()} forwarding to: $url")

    /*
    val echoServer = { r: Request ->
        println("Echo: $r")
        Response(Status.OK)
            .header("Content-type", ContentType.TEXT_PLAIN.value)
            .body("Hello from: `${r.uri.path}` on `${r.uri.host}`")
    }.asServer(SunHttp(6060)).start()
    println("Echo server started on " + echoServer.port())
   */
}

data class IncomingHttpRequest(val uri: Uri, val status: Int, val duration: Long) : Event {
    private val template = "| status: %s | duration: %5d ms | url: %s"
    override fun toString(): String {
        return template.format(status, duration, uri)
    }
}