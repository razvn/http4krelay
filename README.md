# http4krelay

A http request forwarder.

Usecase: 
> GitLab server want to send a webhook to a server `https://mattermost.home/hooks/1234` that it can't reach.
 Instead it will send this request to this application `http://relayapp:1234/hooks/1234`. The app is set up to 
 forward requests to `https://mattermost.home` (environnement variable: `REMOTE_URL`) and it will make the request and send back the response.

- Gradle Kotlin DSL
- http4k
    - ApacheServer
    - ApacheClient
- Java 11 / GraalVN
- Junit 

## Package
```
./gradlew shadowJar
```

## Build native image

### Install GraalVM

- install GraalVM JDK (using [sdkman](https://sdkman.io) or [manually](https://www.graalvm.org))
- install Native image tool using `gu`
```
$GRAALVM/bin/gu native-image
```

### Launch the build
(don't forget to build the app before)

Run from `build/lib`
```
$GRAALVM/bin/native-image -jar http4krelay.jar --no-fallback --enable-https
```


## Running the app

### Environnement variables
The  app uses these env vars:
- `PORT` - port on which the server will be started (defaulted to: `8080`)
- `REMOTE_URL` - remote server to which the request will be forwarded (**required** - without this the app does not start, ex: `https://myserver:8888`)
- `VALID_HOOKS` - list of last path elements (separated by `,`) of the requests (defaulted to: `` (empty) = no filter all requests are forwarded). 
This allows you to filter request to some path for ex if want to allow requests to `https://myserver:8888/hooks/aaaa` and `https://myserver:8888/hooks/bbbb`, 
set this value to `aaaa,bbbb`. Others paths will not be allowed. 

### Running native image:
Just run the native image created:
Run from `build/lib`
```
./http4krelay
```

### Running the java jar:
Run from `build/lib`
```
java - jar http4krelay.jar
```
